import os
import socket
from threading import Thread
from Queue import Queue
from const import *

###########################################################
class Uart(object):
    '''
    Defines a class for initiating UART handle that allows and external
    program to emulate read/write events to a mote via a socket interface.
    '''
    def __init__(self, mote):
        self.mote = mote
        self.socket = None
        self.conn = None
        self.ostream = None
        self.queue = Queue(maxsize=256)

    def setOutputStream(self, ostream):
        '''
        Redirect all data from mote's UART to the specified output stream
        (i.e., any object with a write() method).  However, this setting has
        no effect if a socket connection is available.
        '''
        self.ostream = ostream

    def moteOutput(self, data):
        '''
        Handle data output by the mote
        '''
        # else if an output stream has been set
        # -> send the data to the output stream
        # else
        # -> 
        if self.conn:
            # Socket connection available, send the data via the socket
            try:
                self.conn.send(''.join(map(chr,data)))
            except:
                pass
        elif self.ostream:
            # Output stream available, send data to output stream
            self.ostream.write(''.join(map(chr,data)))
        else:
            # Put data in a size-limited queue
            for x in data:
                if self.queue.full(): self.queue.get()
                self.queue.put(x)

    def read(self, size=1):
        '''
        Blocks and tries to reads size amount of data from mote's UART
        '''
        data = []
        for i in range(size):
            data.append(self.queue.get())
        return ''.join(map(chr,data))

    def write(self, data):
        '''
        Sends the given data to mote's UART interface
        '''
        if type(data) == str:
            data = [ord(x) for x in data]
        self.mote._sendToMoteProc(data, type=Command.CMD_UART_INPUT)

    def setTimeout(self, timeout):
        '''
        TODO: set UART access timeout
        '''
        pass

    def activateSocket(self, address='', port=0):
        '''
        Create a network socket and connects it to the read and write streams.
        A thread will be created to handle incoming data.

        @param address
        Interface address for listening.  Zero means all interfaces.

        @param port
        Listening port.  Zero means any port.

        @return
        A tuple (address,port).
        '''
        # do nothing if socket has already been open
        if self.socket is not None:
            return None

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((address, port))
        self.socket.listen(1) # one client at a time

        # dedicate socket handling task to another thread
        sock_thread = Thread(target=self._handleSocket)
        sock_thread.setDaemon(True)
        sock_thread.start()

        return self.socket.getsockname()

    def _handleSocket(self):
        '''
        Enter a forever-loop to handle connection establishment and interface
        with the read/write streams
        '''
        while True:
            # no current connection
            self.conn = None

            # wait for TCP connection request from external program
            self.conn, (addr,port) = self.socket.accept()

            # wait for data from the socket and pass them all to mote's UART
            try:
                while True:
                    byte = self.conn.recv(1)
                    if len(byte) > 0:
                        self.write(byte)
                    else:
                        break;
            except:
                pass


###########################################################
class Storage(object):
    '''
    Defines a class for managing persistent storage installed with a node
    '''
    def __init__(self, path):

        self.path = path

        if path is not None:
            # determine whether the path is a writable file
            if not os.path.isfile(path) or not os.access(path, os.W_OK):
                raise Exception('File "%s" is not writable' % path)

    def size(self):
        '''
        Return the total size of the storage
        '''
        if self.path:
            return os.path.getsize(self.path)
        else:
            return 0

    def write(self, addr, byte):
        '''
        Write a single byte to the specified location
        '''
        if self.path and addr < self.size():
            f = open(self.path, 'r+b')
            f.seek(addr)
            f.write(chr(byte))
            f.close()

    def read(self, addr):
        '''
        Read a single byte value at the specified location
        '''
        if self.path and addr < self.size():
            f = open(self.path, 'r+b')
            f.seek(addr)
            data = f.read(1)
            f.close()
            return ord(data)
        else:
            return 0


