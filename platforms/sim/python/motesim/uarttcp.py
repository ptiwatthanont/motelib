import sys
import socket
import serial
from threading import Thread
from motesim import LostConnectionException

############################################################
class UartTcp(object):

    ################################################
    def __init__(self, uart_dev, ip, port, baud=9600):
        self.socket = None
        self.serial = None
        self.uart_dev = uart_dev
        self.uart_baud = baud
        self.sock_addr = (ip,port)

    ################################################
    def uartToTcp(self):
        while True:
            data = self.serial.read(1)
            self.socket.send(data)

    ################################################
    def tcpToUart(self):
        while True:
            data = self.socket.recv(1)
            if len(data) == 0:  # socket disconnected
                break
            self.serial.write(data)

    ################################################
    def run(self):
        # open TCP socket
        self.socket = socket.socket()
        self.socket.connect(self.sock_addr)

        # open serial port
        self.serial = serial.Serial(self.uart_dev, baudrate=self.uart_baud)
        self.serial.open()

        self.sock_thread = Thread(target=self.tcpToUart)
        self.sock_thread.setDaemon(True)
        self.sock_thread.start()

        self.uartToTcp()
