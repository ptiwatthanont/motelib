#include <motelib/uart.h>
#include <motelib/cirbuf.h>
#include <motelib/sys/uart.h>
#include "main.h"

static uint8_t txEnabled, rxEnabled;

////////////////////////////////////////////
void _platform_uart_init()
{
    txEnabled = 0;
    rxEnabled = 0;
}

////////////////////////////////////////////
void _platform_uart_enable(bool tx, bool rx)
{
    txEnabled = tx;
    rxEnabled = rx;
}

////////////////////////////////////////////
void _uart_receive(uint8_t *data, uint8_t len)
{
    uint8_t i;

    if (!rxEnabled) return;

    for (i = 0; i < len; i++)
    {
        // If the buffer is already full, remove the oldest byte
        if (cirbufFull(&_sys_uart_cb_in))
            cirbufReadByte(&_sys_uart_cb_in);

        // Append the recently read byte into the buffer
        cirbufWriteByte(&_sys_uart_cb_in, data[i]);

        // Notify sys if marker watch is in progress and a specified marker is
        // encounter
        if (_sys_uart_useMarker && (data[i] == _sys_uart_marker))
            _sys_uart_notifyMarkerFound();
    }
}

////////////////////////////////////////////
void _uart_checkOutputBuffer()
{
	char buf[UART_OUTPUT_BUF_SIZE];
	uint16_t len;

	if (cirbufEmpty(&_sys_uart_cb_out) || !txEnabled) return;

	len = cirbufRead(&_sys_uart_cb_out, buf, UART_OUTPUT_BUF_SIZE);
    _send_to_sim(CMD_UART_OUTPUT, buf, len);
}
