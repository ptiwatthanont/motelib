#include <motelib/led.h>
#include "main.h"

static uint8_t ledStatus[NUM_LEDS];

void _platform_led_init()
{
    uint8_t i;
    for (i = 0; i < NUM_LEDS; i++)
        ledStatus[i] = 0;
}

void ledSet(uint8_t no, uint8_t status)
{
    uint8_t buf[2];
    if (no < NUM_LEDS)
    {
        status = (status != 0);
        ledStatus[no] = status;
        buf[0] = no;
        buf[1] = status;
        _send_to_sim(CMD_LED, buf, 2);
    }
}

uint8_t ledGet(uint8_t no)
{
    if (no < NUM_LEDS)
        return ledStatus[no];
    else
        return -1;
}

void ledToggle(uint8_t no)
{
    if (no < NUM_LEDS) ledSet(no, ledStatus[no]^1);
}
