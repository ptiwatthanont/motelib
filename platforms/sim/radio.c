#include <string.h>

#include "main.h"
#include <motelib/radio.h>
#include <motelib/sys/radio.h>

void _platform_radio_init()
{
}

/*
 * Message format
 *
 * +---------+-----+-----+---------+--------+----------+
 * | FrameID | Src | Dst | DataLen | Header |  Data... |
 * +---------+-----+-----+---------+--------+----------+
 *     (1)     (2)   (2)     (1)       (m)        (n)
 *   
 */
void _platform_radio_send(
        Address dst,
        MessageHeader *header,
        const void *data, 
        uint8_t len)
{
    uint8_t buf[MAX_CMD_LEN];
    uint8_t *ptr = buf;
    Address src = getAddress();

    // FrameID
    *ptr++ = header->seqno;

    // Src
    memcpy(ptr, &src, sizeof(Address));
    ptr += sizeof(Address);

    // Dst
    memcpy(ptr, &dst, sizeof(Address));
    ptr += sizeof(Address);

    // Len
    *ptr++ = len;

    // Header
    memcpy(ptr, header, sizeof(MessageHeader));
    ptr += sizeof(MessageHeader);

    // Data
	if (len) memcpy(ptr, data, len);

    _send_to_sim(CMD_RADIO_SEND, buf, len+(ptr-buf));
}

void _radio_receive(uint8_t *msg)
{
    uint8_t *ptr = msg;
    Address src, dst;
    uint16_t len;
    uint8_t frameId;

    frameId = ptr[0];
    ptr++;

    memcpy(&src, ptr, sizeof(Address));
    ptr += sizeof(Address);

    memcpy(&dst, ptr, sizeof(Address));
    ptr += sizeof(Address);

    len = ptr[0] + sizeof(MessageHeader);
    ptr += 1;

    // the very last byte contains RSSI (0-255)
    memcpy(&_platform_radio_rxStatus.rssi, ptr+len, 1);  // XXX: not checksum?

    if ((dst == getAddress() || dst == BROADCAST_ADDR))
        _sys_radio_receiveDone(src, ptr, len);

}
