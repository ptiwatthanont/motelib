#include "main.h"
#include <motelib/sys/storage.h>

static bool busy;
static uint8_t *readByte;
StorageAddr _platform_storage_size;

//////////////////////////////////////////////////
void _storage_init()
{
    busy = false;
    readByte = NULL;
    _platform_storage_size = 0;
}

//////////////////////////////////////////////////
void _storage_write_done()
{
    busy = false;
}

//////////////////////////////////////////////////
void _storage_read_done(uint8_t data)
{
    busy = false;
    if (readByte != NULL)
        *readByte = data;
}

//////////////////////////////////////////////////
bool _platform_storage_is_ready()
{
    return !busy;
}

//////////////////////////////////////////////////
void _platform_storage_write_byte(StorageAddr addr, uint8_t byte)
{
    char buf[3];  // addr (2) + data (1)
    if (busy) return;

    busy = true;
    buf[0] = addr%256;
    buf[1] = addr/256;
    buf[2] = byte;

    // Send write request to sim
    _send_to_sim(CMD_STORAGE_WRITE_REQ, buf, 3);
}

//////////////////////////////////////////////////
void _platform_storage_read_byte(StorageAddr addr, uint8_t *byte)
{
    if (busy) return;

    busy = true;
    readByte = byte;  // store location to be filled by 'read done'

    // Send read request to sim
    _send_to_sim(CMD_STORAGE_READ_REQ, (char*)&addr, 2);
}
