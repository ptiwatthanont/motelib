#ifndef __PLATFORM_H__
#define __PLATFORM_H__

typedef enum sensor_type_enum
{
    LIGHT_SENSOR = 4,
    TEMP_SENSOR = 5,
    VCC_SENSOR = 3
} SensorType;

typedef enum actor_type_enum
{
    ACTOR0,
    ACTOR1,
    ACTOR2,
    ACTOR3,
    ACTOR4
} ActorType;

typedef enum led_enum
{
	YELLOW,
	GREEN,
	RED
}LED;

typedef struct
{
    uint8_t cca_fail : 1;
    uint8_t no_ack : 1;
} TxStatus;

typedef struct
{
    uint8_t rssi;
} RxStatus;

#endif
