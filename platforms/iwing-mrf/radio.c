#include <string.h>
#include <stddef.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/delay.h>

#include <motelib/system.h>
#include <motelib/radio.h>
#include <motelib/sys/main.h>
#include <motelib/sys/radio.h>

#include "platform.h"
#include "mrf24.h"
#include "IEEE802154.h"
#include "main.h"

//////////////////////////////////
// SPI-related macros & settings
//////////////////////////////////
#define SPI_PORT  PORTB
#define SPI_DDR   DDRB
#define SPI_SS    PINB2   // Slave Select
#define SPI_MOSI  PINB3
#define SPI_MISO  PINB4
#define SPI_SCK   PINB5

#define SPI_ENABLE_SS()   CLR_BIT(SPI_PORT,SPI_SS)  // Enable slave
#define SPI_DISABLE_SS()  SET_BIT(SPI_PORT,SPI_SS)  // Disable slave
#define SPI_ENABLE_INT()  SET_BIT(SPCR,SPIE)  // Enable SPI Interrupt
#define SPI_DISABLE_INT() CLR_BIT(SPCR,SPIE)  // Disable SPI Interrupt

//////////////////////////////////////////////////////////////////////
// Radio hardware/software settings
//////////////////////////////////////////////////////////////////////
#define MRF24_RST_DDR     DDRB
#define MRF24_RST_PORT    PORTB
#define MRF24_RST_PIN     PORTB1

#define MRF24_INT_DDR     DDRB
#define MRF24_INT_PORT    PINB
#define MRF24_INT_PIN     PINB0
#define MRF24_INT_EN_BIT  PCIE0
#define MRF24_INT_MSK_REG PCMSK0
#define MRF24_INT_MSK_BIT PCINT0

#define MRF24_DEF_CHANNEL(ch) (0x2|((ch-11)<<4))


//////////////////////////////////////////////////////////////////////
// Global and external variables
//////////////////////////////////////////////////////////////////////
static char m_rxBuffer[PLATFORM_MAX_MSG_LEN]; ///< Buffer to hold RX frame
static uint8_t m_txncon;        ///< TXNCON register value for the next tx

//////////////////////////////////////////////////////////////////////
// Function Prototypes
//////////////////////////////////////////////////////////////////////
static void readRxFifo();

//////////////////////////////////////////////////////////////////////
// SPI utility functions
//////////////////////////////////////////////////////////////////////

/**
 * Initializes ATMega328P's SPI hardware
 */
static void SPI_Init()
{
    // Set MOSI, SCK and ENB output, all other input
    SPI_DDR = (1<<SPI_MOSI) |
              (1<<SPI_SCK)  |
              (1<<SPI_SS)   |
              (0<<SPI_MISO);

    SPCR = (0<<SPIE) |  // Diable SPI interrupt
           (1<<SPE)  |  // Enable SPI
           (0<<DORD) |  // MSB first
           (1<<MSTR) |  // Master mode
           (0<<CPOL) |  // SPI Mode 0
           (0<<CPHA);
#if MRF24_SCLK_DIV == 4
    SPSR  = (0<<SPI2X);
    SPCR |= (0<<SPR1) | (0<<SPR0);
#elif MRF24_SCLK_DIV == 16
    SPSR  = (0<<SPI2X);
    SPCR |= (0<<SPR1) | (1<<SPR0);
#elif MRF24_SCLK_DIV == 64
    SPSR  = (0<<SPI2X);
    SPCR |= (1<<SPR1) | (0<<SPR0);
#elif MRF24_SCLK_DIV == 128
    SPSR  = (0<<SPI2X);
    SPCR |= (1<<SPR1) | (1<<SPR0);
#elif MRF24_SCLK_DIV == 2
    SPSR  = (1<<SPI2X);
    SPCR |= (0<<SPR1) | (0<<SPR0);
#elif MRF24_SCLK_DIV == 8
    SPSR  = (1<<SPI2X);
    SPCR |= (0<<SPR1) | (1<<SPR0);
#elif MRF24_SCLK_DIV == 32
    SPSR  = (1<<SPI2X);
    SPCR |= (1<<SPR1) | (0<<SPR0);
#else
#error Unsupported value of MRF24_SCLK_DIV
#endif

    SPI_DISABLE_SS();
}

/**
 * Sends a byte to SPI slave
 *
 * @param cData Data byte to be sent
 *
 * @return Data byte read back from slave
 */
static uint8_t SPI_Write(uint8_t cData)
{
    SPDR = cData;

    // Wait for transmission complete
    while (!(SPSR & (1<<SPIF)))
        ;
    return SPDR;
}

/**
 * Reads from SPI slave by sending a dummy 0xFF byte
 *
 * @return Data byte read back from slave
 */
#define SPI_Read() SPI_Write(0xFF)

//////////////////////////////////////////////////////////////////////
// Utility functions for controlling MRF24J40 chip
//////////////////////////////////////////////////////////////////////

/**
 * Reads short-address register from MRF24
 * @param address Address of MRF24's register whose value to be read
 * @return Value of the specified register
 */
static uint8_t readRamShortAddress(ShortRegAddr address)
{
    SPI_ENABLE_SS();
    SPI_Write(address<<1);
    uint8_t value = SPI_Read();
    SPI_DISABLE_SS();
    return value;
}

/**
 * Reads long-address register from MRF24
 * @param address Address of MRF24's register whose value to be read
 * @return Value of the specified register
 */
static uint8_t readRamLongAddress(LongRegAddr address)
{
    SPI_ENABLE_SS();
    SPI_Write(((((uint16_t)address)>>3) & 0x7F) | 0x80);
    SPI_Write( (address<<5) & 0xE0);
    uint8_t value = SPI_Read();
    SPI_DISABLE_SS();
    return value;
}

/**
 * Writes value to MRF24's long-address register
 * @param address Address of MRF24's register whose value to be written
 * @param value Byte value to be written to the register
 */
static void writeRamLongAddress(LongRegAddr address,uint8_t value)
{
    SPI_ENABLE_SS();
    SPI_Write(((((uint16_t)address)>>3) & 0x7F) | 0x80);
    SPI_Write(((address<<5) & 0xE0) | 0x10);
    SPI_Write(value);
    SPI_DISABLE_SS();
}

/**
 * Writes value to MRF24's short-address register
 * @param address Address of MRF24's register whose value to be written
 * @param value Byte value to be written to the register
 */
static void writeRamShortAddress(ShortRegAddr address,uint8_t value)
{
    SPI_ENABLE_SS();
    SPI_Write((address<<1) | 0x01);
    SPI_Write(value);
    SPI_DISABLE_SS();
}

/**
 * Write bytes to MRF24J40's RAM.
 * @param startRamAddr Starting address to write to
 * @param data Pointer to buffer storing data to write
 * @param len Number of bytes to read
 */
static void writeToMrf24Ram(uint16_t startRamAddr, uint8_t* data, uint8_t len)
{
    uint8_t i;
    for (i = 0; i < len; i++)
        writeRamLongAddress(startRamAddr+i, data[i]);
}

/**
 * Read bytes from MRF24J40's RAM into uC's RAM.
 * @param startRamAddr Starting address to read from
 * @param data Pointer to buffer for storing data
 * @param len Number of bytes to read
 */
static void readFromMrf24Ram(uint16_t startRamAddr, uint8_t* data, uint8_t len)
{
    uint8_t i;
    for (i = 0; i < len; i++)
        data[i] = readRamLongAddress(startRamAddr+i);
}

/**
 * Disables Tx and Rx interrupt from MRF24 and prevents receiving packets off
 * the air
 */
static inline void disableTxRxInterrupt()
{
    writeRamShortAddress(INTCON,0xFF); //Disable RX and TX interrupt of MRF24J40
    writeRamShortAddress(BBREG1,0x04); //Disable receiving packets off the air
}

/**
 * Enables Tx and Rx interrupt from MRF24 and starts receiving packets off
 * the air
 */
static inline void enableTxRxInterrupt()
{
    writeRamShortAddress(INTCON,0xF6); //Enable RX and TX interrupt of MRF24J40
    writeRamShortAddress(BBREG1,0x00); //Enable receiving packets
}

/**
 * Initializes MRF24J40 registers
 */
static void MRF24_Init(
        uint16_t node_addr,
        uint16_t pan_id,
        uint8_t channel,
        uint8_t high_power,
        uint8_t txpwr_ctrl)
{
    SPI_DISABLE_INT();

    writeRamShortAddress(SOFTRST,0x07);  //Perform software reset
    writeRamShortAddress(PACON2, 0x98);  //FIFOEN = 1, TXONTS = 0x6
    writeRamShortAddress(TXSTBL, 0x95);  //RFSTBL = 0x9
    writeRamLongAddress (RFCON1, 0x01);  //VCOOPT = 0x1
    writeRamLongAddress (RFCON2, 0x80);  //PLLEN = 1
    writeRamLongAddress (RFCON6, 0x90);  //TXFIL = 1,20MRECVR = 1
    writeRamLongAddress (RFCON7, 0x80);  //SLPCLKSEL = 0x2
    writeRamLongAddress (RFCON8, 0x10);  //RFVCO = 1
    writeRamLongAddress (SLPCON1,0x21);  //CLKOUTEN = 1,SLPCLKDIV = 0x1
    writeRamShortAddress(ORDER,  0xFF);  //BO and SO = 15
    writeRamShortAddress(RXMCR,  0x04);  //Configure as coordinator
    writeRamShortAddress(BBREG2, 0x80);  //Set CCA mode to ED
    writeRamShortAddress(CCAEDTH,0x60);  //Set CCA ED threshold
    writeRamShortAddress(BBREG6, 0x40);  //Set appended RSSI value to FIFO
    writeRamShortAddress(INTCON, 0xF6);  //Enable RX and TX interrupt
    writeRamLongAddress (RFCON3, txpwr_ctrl); //TX-Power control

    if (high_power)
    {
        writeRamLongAddress (TESTMODE,0x0F); //Enable external PA/LNA

        // Enable regulator to PA in MRF24J40MC
        writeRamShortAddress(TRISGPIO,1<<3); //GPIO3 -> Output
        writeRamShortAddress(GPIO    ,1<<3); //GPIO3 = 1
    }

    // Set channel, PAN-ID and node address
    writeRamLongAddress (RFCON0, MRF24_DEF_CHANNEL(channel));
    writeRamShortAddress(PANIDL,LO_BYTE(pan_id));
    writeRamShortAddress(PANIDH,HI_BYTE(pan_id));
    writeRamShortAddress(SADRL, LO_BYTE(node_addr));
    writeRamShortAddress(SADRH, HI_BYTE(node_addr));

    // Read back node address and PAN ID
    _platform_address = (readRamShortAddress(SADRH)<<8) | readRamShortAddress(SADRL);
    _platform_panid   = (readRamShortAddress(PANIDH)<<8) | readRamShortAddress(PANIDL);

    //Reset state machine
    writeRamShortAddress(RFCTL,  0x04);
    writeRamShortAddress(RFCTL,  0x00);
}

/**
 * Initializes radio subsystem.  Called by main module before entering the
 * main loop.
 */
void _platform_radio_init()
{
    uint8_t high_power, txpwr_ctrl;

    SPI_Init();

    // Make interrupt pin input
    CLR_BIT(MRF24_INT_DDR, MRF24_INT_PIN);

    // Make reset pin output and pull it low for 1 ms, then pull high
    SET_BIT(MRF24_RST_DDR, MRF24_RST_PIN);
    CLR_BIT(MRF24_RST_PORT, MRF24_RST_PIN);
    _delay_ms(1);
    SET_BIT(MRF24_RST_PORT, MRF24_RST_PIN);
    _delay_ms(1);

    // Enable pin change interrupt for MRF24
    SET_BIT(PCICR, MRF24_INT_EN_BIT);
    SET_BIT(MRF24_INT_MSK_REG, MRF24_INT_MSK_BIT);

    // Retrieve intial node address, PAN ID, and channel
    _platform_address = eeprom_read_word(EEPROM_NODE_ADDR);
    _platform_panid   = eeprom_read_word(EEPROM_PAN_ID);
    _platform_channel = eeprom_read_byte(EEPROM_RF_CHANNEL);
    high_power = eeprom_read_byte(EEPROM_HIGH_POWER_RF) == 1;
    txpwr_ctrl = eeprom_read_byte(EEPROM_TX_POWER);

    // Convert txpwr_ctrl setting from EEPROM into an appropriate value for
    // RFCON3.  Valid range of the value stored in EEPROM is 0-31, where 0
    // means lowest power and 31 means highest power.
    if (txpwr_ctrl > 31) txpwr_ctrl = high_power ? 26 : 31;
    txpwr_ctrl = (31-txpwr_ctrl) << 3;

    // Initialize MRF24 hardware
    MRF24_Init(
            _platform_address,
            _platform_panid,
            _platform_channel,
            high_power,
            txpwr_ctrl);
}

/**
 * Handles interrupts from MRF24J40.  To be called from the main loop
 */
void Radio_CheckInterrupt()
{
    Mrf24IntStat intStat;

    intStat.flat = readRamShortAddress(INTSTAT);

    if (intStat.bits.txnif) // TX Normal FIFO interrupt
    {
        Mrf24TxStat txStat;
        txStat.flat = readRamShortAddress(TXSTAT);

        // Store tx status in case app wants to know
        _platform_radio_txStatus.num_retries = txStat.bits.txnretry;
        _platform_radio_txStatus.cca_fail    = txStat.bits.ccafail;

        // Report status of latest transmission
        _sys_radio_sendDone(txStat.bits.txnstat == 1 ? RADIO_FAILED : RADIO_OK);
    }

    if (intStat.bits.rxif) // RX FIFO interrupt
    {
        disableTxRxInterrupt();
        readRxFifo();
        enableTxRxInterrupt();
    }
}

/**
 * Starts sending data previously loaded into TX Normal FIFO.  To be called
 * from the main loop.
 */
void Radio_StartTx()
{
    writeRamShortAddress(TXNCON, m_txncon);
}

/**
 * Reads frame data from RX FIFO after receiving RX interrupt
 */
static void readRxFifo()
{
    uint8_t frameLen;
    uint8_t payloadLen;
    uint16_t fifoAddr;
    Mrf24Header mrf24Hdr;

    // read the entire RX frame's length
    frameLen = readRamLongAddress(MRF24_RX_NORMAL_FIFO);

    // read the header up until the destination address field
    readFromMrf24Ram(
            MRF24_RX_NORMAL_FIFO+1,
            (uint8_t*)&mrf24Hdr, 
            offsetof(Mrf24Header, dst) + sizeof(uint16_t));

    // calculate the length of payload
    //   payloadLen = frameLen - headerLen - 2 (LQI/RSSI)
    payloadLen = frameLen - sizeof(Mrf24Header) - 2;

    // Check payload length to prevent buffer overflow
    if (payloadLen > PLATFORM_MAX_MSG_LEN) return;

    // determine FIFO address for src addr field,
    fifoAddr = MRF24_RX_NORMAL_FIFO + offsetof(Mrf24Header, src) + 1;
    if (!mrf24Hdr.frame_ctrl.bits.intrapan)
        fifoAddr += 2; // src PAN is included when intrapan flag is not set

    // read the source address
    readFromMrf24Ram(fifoAddr, (uint8_t*)&mrf24Hdr.src, sizeof(mrf24Hdr.src));
    fifoAddr += sizeof(mrf24Hdr.src);

    // read payload
    readFromMrf24Ram(fifoAddr, (uint8_t*)&m_rxBuffer, payloadLen);
    fifoAddr += payloadLen;

    // store RSSI and LQI in metadata
    fifoAddr += 2;  // skip FCS

    // update most recent RX status
    _platform_radio_rxStatus.lqi  = readRamLongAddress(fifoAddr++);
    _platform_radio_rxStatus.rssi = readRamLongAddress(fifoAddr++);

    // Report successful reception
    _sys_radio_receiveDone(mrf24Hdr.src, m_rxBuffer, payloadLen);
}

/**
 * Puts MRF24J40 to sleep immediately
 */
void mrf24Sleep()
{
	// set state machine to normal mode
	writeRamLongAddress(TESTMODE, 0x00);

	// GPIO1-3 -> Output
	writeRamShortAddress(TRISGPIO,(1<<3)|(1<<2)|(1<<1));

	// Disable PA/LNA, as well as PA regulator
	writeRamShortAddress(GPIO,0);

    writeRamShortAddress(WAKECON, 1<<7); // wake immediately mode
    writeRamShortAddress(SOFTRST, 1<<2); // power management reset
    writeRamShortAddress(SLPACK , 1<<7); // sleep!
}

//////////////////////////////////////////////////////////////////////
// Radio API Implementation
//////////////////////////////////////////////////////////////////////

/**
 * (Platform-specific function)
 */
void _platform_radio_send(
        Address dst,
        MessageHeader *header,
        const void *data,
        uint8_t len)
{
    Mrf24Header mrf24Hdr;

    // Prepare frame header
    mrf24Hdr.frame_ctrl.flat = 0; // reset FCF
    mrf24Hdr.frame_ctrl.bits.frame_type    = IEEE154_TYPE_DATA;
    mrf24Hdr.frame_ctrl.bits.intrapan      = 1;
    mrf24Hdr.frame_ctrl.bits.ack_request   = (dst == BROADCAST_ADDR) ? 0 : 1;
    mrf24Hdr.frame_ctrl.bits.dst_addr_mode = IEEE154_ADDR_SHORT;
    mrf24Hdr.frame_ctrl.bits.src_addr_mode = IEEE154_ADDR_SHORT;

    mrf24Hdr.frame_id = header->seqno;
    mrf24Hdr.dst      = dst;
    mrf24Hdr.dstpan   = _platform_panid;
    mrf24Hdr.src      = _platform_address;

    // Load header's and frame's lengths into TX Normal FIFO
    writeRamLongAddress(MRF24_TX_NORMAL_FIFO, sizeof(Mrf24Header));
    writeRamLongAddress(MRF24_TX_NORMAL_FIFO+1,
            sizeof(Mrf24Header)+sizeof(MessageHeader)+len);

    // Load IEEE802.15.4 header into TX Normal FIFO
    writeToMrf24Ram(MRF24_TX_NORMAL_FIFO+2, 
            (uint8_t*)&mrf24Hdr, sizeof(Mrf24Header));

    // Load message header into TX Normal FIFO
    writeToMrf24Ram(MRF24_TX_NORMAL_FIFO+2+sizeof(Mrf24Header), 
            (uint8_t*)header, sizeof(MessageHeader));

    // Load data into TX Normal FIFO
	if (len)
	{
		writeToMrf24Ram(
				MRF24_TX_NORMAL_FIFO+2+sizeof(Mrf24Header)+sizeof(MessageHeader), 
				(uint8_t*)data, len);
	}

    // Prepare control register to later tell MRF24J40 to start transmitting
    m_txncon = (1 << 0) | (mrf24Hdr.frame_ctrl.bits.ack_request << 2);

    // Inform main loop to trigger tx when it has a chance
    FLAG_ATOMIC(f_radioTxFifoLoaded);
}

//////////////////////////////////
// Interrupt service routines
//////////////////////////////////

/**
 * Pin Change interrupt handler for detecting interrupt signal from MRF24J40
 */
ISR(PCINT0_vect)
{
    // Accept only falling edge
    if (!BIT_SET(MRF24_INT_PORT, MRF24_INT_PIN))
    {
        FLAG_NONATOMIC(f_radioInterrupt);
    }
}
