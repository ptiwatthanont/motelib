/*
 * Modified from USB-232 by Osamu Tamura
 */

#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include "usbdrv.h"

#include <motelib/uart.h>
#include <motelib/cirbuf.h>
#include <motelib/sys/uart.h>

#include "main.h"

#ifndef BAUD
#define BAUD 9600
#endif

#define HW_CDC_BULK_OUT_SIZE 8
#define HW_CDC_BULK_IN_SIZE  8

typedef union usbDWord
{
    uint32_t dword;
    uint8_t  bytes[4];
} usbDWord_t;

enum
{
    SEND_ENCAPSULATED_COMMAND = 0,
    GET_ENCAPSULATED_RESPONSE,
    SET_COMM_FEATURE,
    GET_COMM_FEATURE,
    CLEAR_COMM_FEATURE,
    SET_LINE_CODING = 0x20,
    GET_LINE_CODING,
    SET_CONTROL_LINE_STATE,
    SEND_BREAK
};

static uchar intr3Status; /* used to control interrupt endpoint transmissions */
static uchar stopbit, parity, databit;
static usbDWord_t baud;
static bool tx_enabled, rx_enabled;
static bool sendEmptyFrame;

static void enableUsbUart();
static void disableUsbUart();

static PROGMEM const char configDescrCDC[] =   /* USB configuration descriptor */
{
    9,          /* sizeof(usbDescrConfig): length of descriptor in bytes */
    USBDESCR_CONFIG,    /* descriptor type */
    67,
    0,          /* total length of data returned (including inlined descriptors) */
    2,          /* number of interfaces in this configuration */
    1,          /* index of this configuration */
    0,          /* configuration name string index */
#if USB_CFG_IS_SELF_POWERED
    (1 << 7) | USBATTR_SELFPOWER,       /* attributes */
#else
    (1 << 7),                           /* attributes */
#endif
    USB_CFG_MAX_BUS_POWER/2,            /* max USB current in 2mA units */

    /* interface descriptor follows inline: */
    9,          /* sizeof(usbDescrInterface): length of descriptor in bytes */
    USBDESCR_INTERFACE, /* descriptor type */
    0,          /* index of this interface */
    0,          /* alternate setting for this interface */
    USB_CFG_HAVE_INTRIN_ENDPOINT,   /* endpoints excl 0: number of endpoint descriptors to follow */
    USB_CFG_INTERFACE_CLASS,
    USB_CFG_INTERFACE_SUBCLASS,
    USB_CFG_INTERFACE_PROTOCOL,
    0,          /* string index for interface */

    /* CDC Class-Specific descriptor */
    5,           /* sizeof(usbDescrCDC_HeaderFn): length of descriptor in bytes */
    0x24,        /* descriptor type */
    0,           /* header functional descriptor */
    0x10, 0x01,

    4,           /* sizeof(usbDescrCDC_AcmFn): length of descriptor in bytes */
    0x24,        /* descriptor type */
    2,           /* abstract control management functional descriptor */
    0x02,        /* SET_LINE_CODING,    GET_LINE_CODING, SET_CONTROL_LINE_STATE    */

    5,           /* sizeof(usbDescrCDC_UnionFn): length of descriptor in bytes */
    0x24,        /* descriptor type */
    6,           /* union functional descriptor */
    0,           /* CDC_COMM_INTF_ID */
    1,           /* CDC_DATA_INTF_ID */

    5,           /* sizeof(usbDescrCDC_CallMgtFn): length of descriptor in bytes */
    0x24,        /* descriptor type */
    1,           /* call management functional descriptor */
    3,           /* allow management on data interface, handles call management by itself */
    1,           /* CDC_DATA_INTF_ID */

    /* Endpoint Descriptor */
    7,           /* sizeof(usbDescrEndpoint) */
    USBDESCR_ENDPOINT,  /* descriptor type = endpoint */
    0x80|USB_CFG_EP3_NUMBER,        /* IN endpoint number */
    0x03,        /* attrib: Interrupt endpoint */
    8, 0,        /* maximum packet size */
    USB_CFG_INTR_POLL_INTERVAL,        /* in ms */

    /* Interface Descriptor  */
    9,           /* sizeof(usbDescrInterface): length of descriptor in bytes */
    USBDESCR_INTERFACE,           /* descriptor type */
    1,           /* index of this interface */
    0,           /* alternate setting for this interface */
    2,           /* endpoints excl 0: number of endpoint descriptors to follow */
    0x0A,        /* Data Interface Class Codes */
    0,
    0,           /* Data Interface Class Protocol Codes */
    0,           /* string index for interface */

    /* Endpoint Descriptor */
    7,           /* sizeof(usbDescrEndpoint) */
    USBDESCR_ENDPOINT,  /* descriptor type = endpoint */
    0x01,        /* OUT endpoint number 1 */
    0x02,        /* attrib: Bulk endpoint */
    8, 0,        /* maximum packet size */
    0,           /* in ms */

    /* Endpoint Descriptor */
    7,           /* sizeof(usbDescrEndpoint) */
    USBDESCR_ENDPOINT,  /* descriptor type = endpoint */
    0x81,        /* IN endpoint number 1 */
    0x02,        /* attrib: Bulk endpoint */
    8, 0,        /* maximum packet size */
    0,           /* in ms */
};


uchar usbFunctionDescriptor(usbRequest_t *rq)
{
    if (rq->wValue.bytes[1] == USBDESCR_DEVICE)
    {
        usbMsgPtr = (uchar *)usbDescriptorDevice;
        return usbDescriptorDevice[0];
    }
    else  /* must be config descriptor */
    {
        usbMsgPtr = (uchar *)configDescrCDC;
        return sizeof(configDescrCDC);
    }
}

////////////////////////////////////////////////////////
void _uart_poll()
{
    static uint8_t buf[HW_CDC_BULK_OUT_SIZE];
    uint8_t len;

    // No USB connection if UART is not required at all
    if (!tx_enabled && !rx_enabled) return;

    usbPoll();

    // Accept more request if RX buffer has enough room
    if (usbAllRequestsAreDisabled()
            && cirbufFree(&_sys_uart_cb_in) >= HW_CDC_BULK_IN_SIZE)
    {
        usbEnableAllRequests();
    }

    // If TX buffer contains data and USB interrupt is ready, request USB
    // interrupt
    if (usbInterruptIsReady())
    {
        len = cirbufRead(&_sys_uart_cb_out, buf, HW_CDC_BULK_OUT_SIZE);
        if (len)
        {
            usbSetInterrupt(buf, len);

            // Data will be sent in complete frame; next time send an empty
            // frame to indicate end of transmission
            if (len == HW_CDC_BULK_OUT_SIZE)
                sendEmptyFrame = true;
        }
        else if (sendEmptyFrame)
        {
            usbSetInterrupt(NULL, 0);
            sendEmptyFrame = false;
        }
    }

#if USB_CFG_HAVE_INTRIN_ENDPOINT3
    /* We need to report rx and tx carrier after open attempt */
    if (intr3Status != 0 && usbInterruptIsReady3())
    {
        static uchar serialStateNotification[10] = {0xa1, 0x20, 0, 0, 0, 0, 2, 0, 3, 0};

        if (intr3Status == 2)
        {
            usbSetInterrupt3(serialStateNotification, 8);
        }
        else
        {
            usbSetInterrupt3(serialStateNotification+8, 2);
        }
        intr3Status--;
    }
#endif
}

////////////////////////////////////////////////////////
void _platform_uart_init()
{
    /*    USART configuration    */
    baud.dword  = BAUD;
    stopbit = 0;
    parity  = 0;
    databit = 8;

    tx_enabled = false;
    rx_enabled = false;
    sendEmptyFrame = false;
    cirbufClear(&_sys_uart_cb_in);
    cirbufClear(&_sys_uart_cb_out);
}

////////////////////////////////////////////////////////
void _platform_uart_enable(bool tx, bool rx)
{
    // If tx or rx is enabled, establish USB connection if previously
    // disconnected
    if ((tx || rx) && (!tx_enabled && !rx_enabled))
        enableUsbUart();

    // If tx and rx are both disabled, disconnect USB if previously connected
    if ((!tx && !rx) && (tx_enabled || rx_enabled))
        disableUsbUart();

    tx_enabled = tx;
    rx_enabled = rx;
}

////////////////////////////////////////////////////////
static void enableUsbUart()
{
    usbInit();

    /* enforce re-enumeration, do this while interrupts are disabled! */
    usbDeviceDisconnect();
    _delay_ms(300);
    usbDeviceConnect();

    intr3Status = 0;
}

////////////////////////////////////////////////////////
static void disableUsbUart()
{
    usbDeviceDisconnect();
}

/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */

uchar usbFunctionSetup(uchar data[8])
{
    usbRequest_t *rq = (void *)data;

    if ((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS)
    {    /* class request type */
        if (rq->bRequest==GET_LINE_CODING || rq->bRequest==SET_LINE_CODING)
        {
            return 0xff;
        }
        if (rq->bRequest == SET_CONTROL_LINE_STATE)
        {
#if USB_CFG_HAVE_INTRIN_ENDPOINT3
            /* Report serial state (carrier detect). On several Unix platforms,
             * tty devices can only be opened when carrier detect is set.
             */
            if (intr3Status == 0)
                intr3Status = 2;
#endif
        }

        /*  Prepare bulk-in endpoint to respond to early termination   */
        if((rq->bmRequestType & USBRQ_DIR_MASK) == USBRQ_DIR_HOST_TO_DEVICE)
            sendEmptyFrame = true;
    }

    return 0;
}

////////////////////////////////////////////////////////
uchar usbFunctionRead( uchar *data, uchar len )
{
    data[0] = baud.bytes[0];
    data[1] = baud.bytes[1];
    data[2] = baud.bytes[2];
    data[3] = baud.bytes[3];
    data[4] = stopbit;
    data[5] = parity;
    data[6] = databit;

    return 7;
}

////////////////////////////////////////////////////////
uchar usbFunctionWrite( uchar *data, uchar len )
{

    ///*    SET_LINE_CODING    */
    baud.bytes[0] = data[0];
    baud.bytes[1] = data[1];
    baud.bytes[2] = data[2];
    baud.bytes[3] = data[3];

    stopbit    = data[4];
    parity     = data[5];
    databit    = data[6];

    if (parity > 2)
        parity = 0;
    if (stopbit == 1)
        stopbit = 0;

    return 1;
}

////////////////////////////////////////////////////////
void usbFunctionWriteOut(uchar *data, uchar len)
{
    // store data from USB into system input buffer
    for (; len; len--)
    {
        cirbufWriteByte(&_sys_uart_cb_in, *data);

        // Notify sys if marker watch is in progress and a specified marker is
        // encounter
        if (_sys_uart_useMarker && (*data == _sys_uart_marker))
            _sys_uart_notifyMarkerFound();

        data++;
    }

    // postpone next requests if system input buffer does not have enough room
    if (cirbufFree(&_sys_uart_cb_in) <= HW_CDC_BULK_IN_SIZE)
        usbDisableAllRequests();
}
