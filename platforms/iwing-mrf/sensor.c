#include <stdbool.h>
#include <avr/io.h> 
#include <avr/interrupt.h> 

#include <motelib/system.h>
#include <motelib/sensor.h>
#include <motelib/led.h>
#include <motelib/cirbuf.h>

#include "platform.h"
#include "main.h"

static SensorDataReady m_callback; ///< Callback pointer for the most recent ADC read
static SensorType m_sensor; ///< Channel no. for the most recent ADC read
static uint16_t m_value; ///< Store current reading
static uint16_t m_recent; ///< Store most recent reading
static bool m_pending;

///////////////////////////////////////////////////////////
void Sensor_Init()
{
    ADCSRA = 0x0E; // ADC diabled, interrupt enabled, prescale 64
    m_sensor = 0xFF;
    m_pending = false;
}

///////////////////////////////////////////////////////////
void Sensor_Callback()
{
    if (m_callback) m_callback(m_value);
    m_recent = m_value;
    m_sensor = 0xFF;
    m_pending = false;
}

///////////////////////////////////////////////////////////
SensorStatus sensorRequestAnalog(SensorType sensor, SensorDataReady readDone)
{
    // Report busy if ADC is currently in used
    if (m_pending) return SENSOR_BUSY;

    // Start reading ADC
    m_sensor   = sensor;
    m_callback = readDone;
    m_pending  = true;
    ADMUX  = (0<<ADLAR); // default Vref = Aref

    // Select appropriate voltage reference
    if (sensor == SENSOR_INT_TEMP)
        ADMUX |= (1<<REFS1) | (1<<REFS0); // Vref = 1.1V
    else if (sensor == SENSOR_VCC)
        ADMUX |= (0<<REFS1) | (1<<REFS0); // Vref = AVcc

    ADMUX |= (sensor & 0x0F); // bit 3..0 select channel 

    ADCSRA |= (1 << ADEN) | (1<<ADSC); // Enable ADC and start conversion !

    return SENSOR_OK;
}

///////////////////////////////////////////////////////////
uint16_t sensorAnalogResult(SensorType sensor)
{
    // TODO: take sensor channel into consideration
    return m_recent;
}

///////////////////////////////////////////////////////////
uint8_t sensorReadDigital(SensorType sensor)
{
    if (sensor > 5) return 0;
    CLR_BIT(DDRC, sensor); // Direction = in
    return BIT_SET(PINC, sensor);
}

///////////////////////////////////////////////////////////
bool sensorAnalogWaiting(SensorType sensor)
{
    return (sensor == m_sensor) & m_pending;
}

///////////////////////////////////////////////////////////
bool sensorAnalogAvailable(SensorType sensor)
{
    return !m_pending;
}

///////////////////////////////////////////////////////////
ISR(ADC_vect)
{
    m_value  = ADCL;
    m_value += (ADCH<<8);    
    CLR_BIT(ADCSRA, ADEN); // Disable ADC
    FLAG_NONATOMIC(f_sensorDataReady);
}

