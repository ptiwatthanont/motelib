#include <jendefs.h>
#include <AppHardwareApi.h>
#include "main.h"
#include <motelib/sys/led.h>

void _platform_led_init()
{
#if defined(JN5168)
    vAHI_DioSetDirection(0, 1<<8|1<<9|1<<10);
    vAHI_DioSetOutput(0, 1<<8|1<<9|1<<10);
#elif defined(JN5148)
    vAHI_DioSetDirection(0, 1<<0|1<<1|1<<2);
    vAHI_DioSetOutput(0, 1<<0|1<<1|1<<2);
#endif
}

uint8_t ledGet(uint8_t no)
{
    if (no > 2) return 0;
#if defined(JN5168)
    no += 8;
#endif
    return (u32AHI_DioReadInput() & (1 << no)) ? 1 : 0;
}

void ledSet(uint8_t no, uint8_t status)
{
    if (no > 2) return;
#if defined(JN5168)
    no += 8;
#endif
    if (status)
        vAHI_DioSetOutput(1<<no, 0);
    else
        vAHI_DioSetOutput(0, 1<<no);
}

void ledToggle(uint8_t no)
{
    ledSet(no, !ledGet(no));
}
