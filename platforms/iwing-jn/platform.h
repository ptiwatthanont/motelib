#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#include <stdint.h>

#define PLATFORM_IWING_JN

// Maximun length of message
#define PLATFORM_MAX_MSG_LEN  100

// Define timer tick length in milliseconds
#define PLATFORM_TIMER_TICK_INTERVAL  16

// Overwrite default printf with customized one
#define printf(fmt,args...) printf_jennic(fmt,##args)

// Overwrite system's debug with printf
#ifndef NDEBUG
#   define debug(fmt,args...) { printf_jennic(fmt,##args); uartWriteByte('\r'); uartWriteByte('\n'); }
#endif

int printf_jennic(char *fmt, ...);

typedef struct
{
} RadioTxStatus;

typedef struct
{
    uint8_t lqi;
} RadioRxStatus;

typedef enum sensor_type_enum
{
    SENSOR_D0 = 0,
    SENSOR_D1,
    SENSOR_D2,
    SENSOR_D3,
    SENSOR_D4,
    SENSOR_D5,
    SENSOR_D6,
    SENSOR_D7,
    SENSOR_D8,
    SENSOR_D9,
    SENSOR_D10,
    SENSOR_D11,
    SENSOR_D12,
    SENSOR_D13,
    SENSOR_D14,
    SENSOR_D15,
    SENSOR_D16,
    SENSOR_D17,

    SENSOR_A1 = 0x81,
    SENSOR_A2,
    SENSOR_A3,
    SENSOR_A4,
    SENSOR_TEMP,  //> On-chip temperature sensor
    SENSOR_VOLT   //> Internal voltage monitor
} SensorType;

typedef enum actor_type_enum
{
    ACTOR_0 = 0,
    ACTOR_1,
    ACTOR_2,
    ACTOR_3,
    ACTOR_4,
    ACTOR_5,
    ACTOR_6,
    ACTOR_7,
    ACTOR_8,
    ACTOR_9,
    ACTOR_10,
    ACTOR_11,
    ACTOR_12,
    ACTOR_13,
    ACTOR_14,
    ACTOR_15,
    ACTOR_16,
    ACTOR_17
} ActorType;

#endif
