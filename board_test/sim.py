from motesim import Simulator, Mote, Gateway

######################################
def activateUart(node):
    (ip,port) = sim.nodes[node].uart.activateSocket()
    print "Node#%d's UART emulated at TCP port %d" % (node,port)

######################################
def interact():
    import code
    console = code.InteractiveConsole({'sim':sim})
    activateUart(0)
    activateUart(1)
    console.interact()

######################################
MOTEAPP = 'build/sim/test.elf'
sim = Simulator()
sim.addNode(Mote(MOTEAPP), (180,100))
sim.addNode(Mote(MOTEAPP), (250,100))
sim.run(script=interact)
