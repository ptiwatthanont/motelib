/** 
 * @file
 * @brief Circular Buffer API
 *
 * Definitions for creating and maintaining circular buffers.
 *
 * @author Chaiporn Jaikaeo <chaiporn.j@ku.ac.th>
 *
 */
#ifndef __MOTELIB_CIRBUF_H__
#define __MOTELIB_CIRBUF_H__

#include <stdint.h>

/**
 * Struct for implementing a circular buffer
 */
typedef struct
{
    uint16_t firstFree; ///< index of the first free byte
    uint16_t size;      ///< total usable size of the buffer
    uint16_t free;      ///< number of remaining free bytes
    char    *buffer;    ///< pointer to the first byte of buffer mem
} CirBuf;

/**
 * Initialize a circular buffer
 *
 * @param cb pointer to a circular buffer struct
 * @param buffer pointer to buffer area
 * @param size size of the buffer to be created
 */
void cirbufInit(CirBuf *cb, void *buffer, uint16_t size);

/**
 * Return the total size of a circular buffer
 * @param cb pointer to a circular buffer struct
 */
#define cirbufSize(cb)  ((cb)->size)

/**
 * Check if the circular buffer is empty
 * @param cb pointer to a circular buffer struct
 * @return true if the buffer is empty; false otherwise
 */
#define cirbufEmpty(cb)  ((cb)->free == (cb)->size)

/**
 * Check if the circular buffer is full
 * @param cb pointer to a circular buffer struct
 * @return true if the buffer is full; false otherwise
 */
#define cirbufFull(cb)  ((cb)->free == 0)

/**
 * Return the number of free bytes left in the buffer
 * @param cb pointer to a circular buffer struct
 */
#define cirbufFree(cb)   ((cb)->free)

/**
 * Return the number of bytes stored in the buffer
 * @param cb pointer to a circular buffer struct
 */
#define cirbufUsed(cb)   ((cb)->size - (cb)->free)

/**
 * Write a single byte to the circular buffer
 *
 * @param cb
 * pointer to a circular buffer struct
 *
 * @param data
 * byte value to be written
 *
 * @retval 0
 * write cannot be done because the buffer is already full
 * @retval 1
 * write is successful
 */
uint8_t cirbufWriteByte(CirBuf *cb, uint8_t data);

/**
 * Write contents to a circular buffer
 *
 * @param cb
 * pointer to a circular buffer struct
 *
 * @param data
 * pointer to the first byte of data
 *
 * @param len
 * number of bytes to be written
 *
 * @return
 * the number of bytes successfully written
 */
uint16_t cirbufWrite(CirBuf *cb, const void *data, uint16_t len);

/**
 * Read a single byte from the circular buffer
 *
 * @param cb
 * pointer to a circular buffer struct
 *
 * @return
 * next data byte from the buffer.  If the buffer is empty, 0 is return.
 */
uint8_t cirbufReadByte(CirBuf *cb);

/**
 * Read contents out of a circular buffer and advance the read pointer
 *
 * @param cb
 * pointer to a circular buffer struct
 *
 * @param data
 * pointer to the first byte of data.  If data is NULL, the contents will
 * simply be removed from the circular buffer
 *
 * @param len
 * number of bytes to be copied
 *
 * @return
 * the number of bytes successfully read out
 */
uint16_t cirbufRead(CirBuf *cb, void *data, uint16_t len);

/**
 * Clear all data from the circular buffer
 *
 * @param cb
 * pointer to a circular buffer struct
 */
void cirbufClear(CirBuf *cb);

#endif
