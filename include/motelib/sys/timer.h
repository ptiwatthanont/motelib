#ifndef __MOTELIB_SYS_TIMER_H__
#define __MOTELIB_SYS_TIMER_H__

#include <stdint.h>
#include "../timer.h"

void _sys_timer_init();
void _platform_timer_init();
void _sys_timer_tick();

#endif

