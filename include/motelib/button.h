/** 
 * @file
 * @brief User Button API
 *
 * Definitions for monitoring and detecting press and release of user
 * button.
 *
 * @author Chaiporn Jaikaeo <chaiporn.j@ku.ac.th>
 */
#ifndef __MOTELIB_BUTTON_H__
#define __MOTELIB_BUTTON_H__

#include <stdbool.h>

/**
 * @typedef ButtonStatus
 * Constants representing button status
 */
typedef enum
{
    BUTTON_PRESSED,   ///< Button is pressed
    BUTTON_RELEASED,  ///< Button is released
} ButtonStatus;

/**
 * Defines the type for a button event handler function.
 */
typedef void (*ButtonHandler)(ButtonStatus status);

/**
 * Checks whether the button is currently pressed
 * @retval true if the button is currently pressed
 * @retval false if the button is currently released
 */
bool buttonIsPressed();

/**
 * Sets a handler for monitoring button events.
 * @param handler Function to handler button events
 */
void buttonSetHandler(ButtonHandler handler);

#endif
