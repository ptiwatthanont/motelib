'''
@package record

Provides classes for defining record structures that allow serialization and
deserialization
'''

from struct import Struct

#################################################
class Field(object):
    '''
    Defines a generic field to be defined in a record
    '''
    typemap = { # fieldtype -> [struct-code,default-value]
            'uint8_t'  : ['B',0],
            'uint16_t' : ['H',0],
            'uint32_t' : ['L',0],
            'int8_t'   : ['b',0],
            'int16_t'  : ['h',0],
            'int32_t'  : ['l',0],
            'Address'  : ['H',0xFFFF],
            }

    fieldCount = 0

    #################################################
    def __init__(self, fieldType, default=None):
        '''
        Defines a field with specified field type and default value
        @param fieldType specifies type of the field.  Supported types are:
        [u]int{8,16,32}_t and Address
        @param default specifies a default value of this field in a record
        '''
        self.index = Field.fieldCount
        self.fieldType = fieldType
        self.code,self.default = self.typemap[fieldType]
        if default:
            self.default = default
        Field.fieldCount += 1


##########################################
class RecordMeta(type):
    '''
    Deals with class alteration when the Record class is defined
    '''
    #################################################
    def __new__(cls, name, bases, dct):

        # Obtains all attributes that are instances of Field
        fieldList = [(a,o) for (a,o) in dct.items() if isinstance(o,Field)]

        # Sort them by their time of creation
        fieldList.sort(key=lambda (a,o): o.index)

        # convert all fields into normal types with their default values
        for fName,fObj in fieldList:
            dct[fName] = fObj.default

        # keep track of all fields in the class
        dct['_fields'] = fieldList

        # return the altered class
        return super(RecordMeta, cls).__new__(cls, name, bases, dct)

#################################################
class Record(object):
    '''
    Serves as a base class for defining record classes.  It makes use of
    metaclass feature in Python.
    '''

    __metaclass__ = RecordMeta

    #################################################
    def __init__(self, **kwargs):
        '''
        Initializes a record with optionally given field values.
        '''
        for (k,v) in kwargs.items():
            if k not in self.fieldNames():
                raise Exception("Field '%s' does not exist" % k)

            # TODO: make sure each given value is acceptable by the
            # corresponding field
            setattr(self, k, v)

        # construct struct-format from the field list, using little-endian
        structFormat = '<' + ''.join([field.code for name,field in self._fields])
        self.struct = Struct(structFormat)

    #################################################
    def __eq__(self, record):
        '''
        Checks field-by-field equality of this record and a given record
        '''
        if not isinstance(record, self.__class__):
            return False

        for f in self.fieldNames():
            if getattr(self,f) != getattr(record,f):
                return False

        return True

    #################################################
    def fieldNames(self):
        '''
        Returns a list of all field names
        '''
        return [n for (n,f) in self._fields]

    #################################################
    def __str__(self):
        return "%s(%s)" % (
                self.__class__.__name__,
                ','.join(
                    [("%s=%d" % (n,getattr(self,n))) 
                        for n in self.fieldNames()])
                )

    #################################################
    def serialize(self):
        '''
        Encodes the entire record into a byte array
        '''
        values = [getattr(self,name) for name,fObj in self._fields]
        return self.struct.pack(*values)

    #################################################
    def deserialize(self, byteStr):
        '''
        Decodes the given byte array and store values into their corresponding
        fields.
        @param data a string packed with data
        '''
        avp = zip(self.fieldNames(), self.struct.unpack(byteStr))
        for attr,value in avp:
            setattr(self, attr, value)

#################################################
if __name__ == '__main__':
    # Testing
    class TestRecord(Record):
        byteVal   = Field('uint8_t')
        charVal   = Field('int8_t')
        ushortVal = Field('uint16_t')
        shortVal  = Field('int16_t')
        ulongVal  = Field('uint32_t')
        longVal   = Field('int32_t')
        address   = Field('Address')

    r1 = TestRecord(
            byteVal   =  3,
            charVal   = -3,
            ushortVal =  1000,
            shortVal  = -1000,
            ulongVal  =  1000000,
            longVal   = -1000000,
            address   = 12345,
            )
    print r1
    data = r1.serialize()
    print data.__repr__()
    r2 = TestRecord()
    r2.deserialize(data)
    print r2
    print r1 == r2
