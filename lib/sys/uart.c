#include <motelib/cirbuf.h>
#include <motelib/uart.h>
#include <motelib/sys/uart.h>

#include <platform.h>

CirBuf  _sys_uart_cb_in;
CirBuf  _sys_uart_cb_out;
char    _sys_uart_marker;
uint8_t _sys_uart_useMarker;

static char inbuf[UART_INPUT_BUF_SIZE];
static char outbuf[UART_OUTPUT_BUF_SIZE];
static UartInputReady reqCallback;
static uint16_t reqLen;

static void resetCallback();

///////////////////////////////////////////////////////
void _sys_uart_init()
{
    resetCallback();

    cirbufInit(&_sys_uart_cb_in, inbuf, UART_INPUT_BUF_SIZE);
    cirbufInit(&_sys_uart_cb_out, outbuf, UART_OUTPUT_BUF_SIZE);
    _platform_uart_init();
}

///////////////////////////////////////////////////////
void uartEnable(bool tx, bool rx)
{
    _platform_uart_enable(tx, rx);
}

///////////////////////////////////////////////////////
uint8_t uartWriteByte(uint8_t data)
{
    return cirbufWriteByte(&_sys_uart_cb_out, data);
}

///////////////////////////////////////////////////////
uint16_t uartWrite(void* data, uint16_t len)
{
    return cirbufWrite(&_sys_uart_cb_out, data, len);
}

///////////////////////////////////////////////////////
void uartWatchInputBytes(uint16_t len, UartInputReady inputReady)
{
    _sys_uart_useMarker = 0;
    reqLen       = len;
    reqCallback  = inputReady;
}

///////////////////////////////////////////////////////
void uartWatchInputMarker(char marker, uint16_t maxLen, UartInputReady inputReady)
{
    _sys_uart_useMarker = 1;
    _sys_uart_marker    = marker;
    reqLen              = maxLen;
    reqCallback         = inputReady;
}

///////////////////////////////////////////////////////
uint16_t uartOutputFree()
{
    return cirbufFree(&_sys_uart_cb_out);
}

///////////////////////////////////////////////////////
uint16_t uartInputLen()
{
    return cirbufUsed(&_sys_uart_cb_in);
}

///////////////////////////////////////////////////////
uint8_t uartReadByte()
{
    return cirbufReadByte(&_sys_uart_cb_in);
}

///////////////////////////////////////////////////////
uint16_t uartRead(void *data, uint16_t len)
{
    return cirbufRead(&_sys_uart_cb_in, data, len);
}

///////////////////////////////////////////////////////
void uartFlushOutput()
{
    cirbufClear(&_sys_uart_cb_out);
}

///////////////////////////////////////////////////////
void uartFlushInput()
{
    cirbufClear(&_sys_uart_cb_in);
}

/**
 * Called by the main loop to poll circular input buffer
 */
void _sys_uart_pollInput()
{
    if (!reqLen && !_sys_uart_useMarker)
    {
        // This condition is equivalent to the situation where no one is
        // waiting
        return;
    }

    if (cirbufUsed(&_sys_uart_cb_in) >= reqLen)
    {
        // clear settings and inform the requester
        UartInputReady clientCallback = reqCallback;
        resetCallback();
        clientCallback(0);
    }
}

/**
 * Called by platform to notify that a specified marker has been encountered
 * and is already in the input buffer
 */
void _sys_uart_notifyMarkerFound()
{
    // clear settings and inform the requester
    UartInputReady clientCallback = reqCallback;
    resetCallback();
    clientCallback(1);
}

///////////////////////////////////////////////////////
static void resetCallback()
{
    reqCallback  = NULL;
    reqLen       = 0;
    _sys_uart_useMarker = 0;
}

