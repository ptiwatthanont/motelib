from motesim import Simulator, Mote
from time import time, sleep
from threading import Thread

######################################
def externalEvents():
    sleep(1)
    motes[0].boot()
    sleep(3)
    motes[1].boot()
    sleep(5)
    motes[2].boot()
    motes[0].shutdown()
    sleep(3)
    motes[0].boot()
    

######################################
sim = Simulator()
motes = []
motes.append(Mote('build/sim/blink.elf', (100,100)))
motes.append(Mote('build/sim/blink.elf', (200,100)))
motes.append(Mote('build/sim/blink.elf', (200,200)))
for i,m in enumerate(motes):
    sim.addNode(m)

sim.run(bootMotes=False, extScript=externalEvents)
