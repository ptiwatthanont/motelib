#include <motelib/system.h>
#include <motelib/led.h>
#include <motelib/timer.h>

Timer t1, t2;

void blink1(Timer *t)
{
    ledToggle(0);
}

void blink2(Timer *t)
{
    ledToggle(1);
}

void boot()
{
    timerCreate(&t1);
    timerStart(&t1, TIMER_PERIODIC, 1000, blink1);
    timerCreate(&t2);
    timerStart(&t2, TIMER_PERIODIC, 500, blink2);
}
