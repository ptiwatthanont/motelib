/** 
 * SDI12 Sensor Interfacing Board
 *
 */

#include <avr/io.h>
#include <util/delay.h>
#include <util/delay_basic.h>

#define RELEASE_BUS_C(pin) DDRC &= ~(1 << pin) // To become input pin
#define CAPTURE_BUS_C(pin) DDRC |=  (1 << pin) // To become output pin
#define RELEASE_BUS_D(pin) DDRD &= ~(1 << pin) // To become input pin
#define CAPTURE_BUS_D(pin) DDRD |=  (1 << pin) // To become output pin

// --------------------------------------------------------
void delay_us(unsigned long int t)
{
//    _delay_us(t);
    _delay_loop_1(t);     
//    _delay_loop_1(t*2);     
//    _delay_loop_2(t*3);
}

void delay_ms(unsigned long int t)
{
//    _delay_ms(t);
    t *= 1000;
    while (t--)
        delay_us(1);        
} 

// --------------------------------------------------------
void output_c_high(uint8_t pin)
{
    CAPTURE_BUS_C(pin);
    PORTC |= (1 << pin); // activate '1'    
    delay_us(10);
}

void output_c_low(uint8_t pin)
{    
    CAPTURE_BUS_C(pin);
    PORTC &= ~(1 << pin); // deactivate '0'
    delay_us(10);
}

uint8_t input_c(uint8_t pin)
{
    return (PINC & (1 << pin)) ? 1 : 0;
} 

// --------------------------------------------------------
void output_d_high(uint8_t pin)
{
    CAPTURE_BUS_D(pin);
    PORTD |= (1 << pin); // activate '1'    
    delay_us(10);
}

void output_d_low(uint8_t pin)
{    
    CAPTURE_BUS_D(pin);
    PORTD &= ~(1 << pin); // deactivate '0'
    delay_us(10);
}

uint8_t input_d(uint8_t pin)
{
    return (PIND & (1 << pin)) ? 1 : 0;
} 

// --------------------------------------------------------



void Uart_Init_SDI12()
{
    #define FOSC 12000000 // Clock Speed
    #define MYBAUD 1200
    #define MYUBRR (FOSC/16/MYBAUD-1)

    /*Set baud rate */
    UBRR0H = (unsigned char)(MYUBRR>>8); 
    UBRR0L = (unsigned char)MYUBRR;    
        
    UCSR0C = (0<<UMSEL00) | (0<<UMSEL01) // Asynchronous mode
        | (0<<UPM00) | (1<<UPM01) // Even
        | (0<<USBS0) // 1 Stop Bit
        | (0<<UCSZ02) | (1<<UCSZ01) | (0<<UCSZ00) // 7 bit size
        | (0<<UCPOL0); // Clock parity Rising:TX Falling:RX. Write to 0 when in Async mode    
}

