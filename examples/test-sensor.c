#include <motelib/system.h>
#include <motelib/led.h>
#include <motelib/timer.h>
#include <motelib/sensor.h>
#include <motelib/radio.h>

#include "platform.h"

Timer t1;

void lightReady(uint16_t value)
{
//    debug("Light reading is %d", value);
	char test[2];
	test[1] = value / 256;
	test[0] = value % 256;
	radioRequestTx(BROADCAST_ADDR, 2, test, 2, NULL);
}

void senseLight(Timer *t)
{
    sensorRequestAnalog(SENSOR_1, lightReady);
}

void boot()
{
    timerCreate(&t1);
    timerStart(&t1, TIMER_PERIODIC, 1000, senseLight);
}
