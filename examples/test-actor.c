#include <motelib/system.h>
#include <motelib/timer.h>
#include <motelib/actor.h>

#include "platform.h"

Timer t1, t2;

void toggleActor(ActorType actor)
{
    if (actorGetState(actor) == 0)
        actorSetState(actor, 1);
    else
        actorSetState(actor, 0);
}

void toggleActor0(Timer *t)
{
    toggleActor(ACTOR_0);
}

void toggleActor1(Timer *t)
{
    toggleActor(ACTOR_1);
}

void boot()
{
    timerCreate(&t1);
    timerCreate(&t2);
    timerStart(&t1, TIMER_PERIODIC, 1000, toggleActor0);
    timerStart(&t2, TIMER_PERIODIC, 2000, toggleActor1);
}
